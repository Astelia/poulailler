package fr.afpa.services;

import java.util.Map;
import java.util.Map.Entry;

import fr.afpa.controles.ControleEleveur;
import fr.afpa.controles.ControleVolaille;
import fr.afpa.entites.Canard;
import fr.afpa.entites.Elevage;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;
import fr.afpa.entites.VolailleComestible;
import fr.afpa.ihm.IHMElevage;
import fr.afpa.ihm.IHMEleveur;
import fr.afpa.ihm.IHMGeneral;
import fr.afpa.ihm.IHMMenu;
import fr.afpa.ihm.IHMPaon;
import fr.afpa.ihm.IHMVolaille;
import fr.afpa.ihm.IHMVolailleComestible;

public class MenuServices {

	private static final String OPERATION_IMPOSSIBLE = "L'operation n'a pas pu etre effectuee.";
	
	/**
	 * Permet de modifier le nom d'un elevage
	 * @param elevage : l'elevage dont on modifie le nom
	 */
	private static void modifierNomElevage(Elevage elevage) {
		IHMElevage ihmE = new IHMElevage();
		ElevageServices es = new ElevageServices();
		IHMMenu.entetesMenu("Modifier le nom de l'elevage");
		System.out.println("Voici l'ancien nom de l'elevage : "+elevage.getNom());
		String nom = ihmE.demanderSaisieNouveauNom(elevage.getScanner());
		if (es.modificationNomDElevage(nom, elevage)) {
			System.out.println("La modification a bien ete effectuee !");
			System.out.println("Le nouveau nom de l'elevage est : "+elevage.getNom());
		}
		else {
			System.out.println(OPERATION_IMPOSSIBLE);
		}
	}
	
	/**
	 * Permet de modifier les informations de l'eleveur
	 * @param elevage : l'elevage ou se trouve l'eleveur a modifier
	 */
	private static void modifierInfosEleveur(Elevage elevage) {
		IHMEleveur ihmE = new IHMEleveur();
		EleveurServices es = new EleveurServices();
		IHMMenu.entetesMenu("Modifier le nom de l'eleveur");
		System.out.println("Voici les anciennes informations de l'eleveur : "+elevage.getEleveur());
		String[] infos = new String[4];
		infos[0] = ihmE.demanderSaisieNom(elevage.getScanner());
		infos[1] = ihmE.demanderSaisiePrenom(elevage.getScanner());
		infos[2] = ihmE.demanderSaisieLogin(elevage.getScanner());
		infos[3] = ihmE.demanderSaisieMDP(elevage.getScanner());
		if (es.modificationInfosEleveur(infos, elevage.getEleveur())) {
			System.out.println("La modification a bien ete effectuee !");
			System.out.println("Voici les nouvelles informations de l'eleveur : "+elevage.getEleveur());
		}
		else {
			System.out.println(OPERATION_IMPOSSIBLE);
		}
	}
	
	/**
	 * Permet d'ajouter une livraison a l'elevage
	 * @param elevage : l'elevage ou l'on souhaite ajouter les volailles
	 */
	private static void livraisonVolailles(Elevage elevage) {
		IHMElevage ihmE = new IHMElevage();
		ControleVolaille cv = new ControleVolaille();
		ElevageServices es = new ElevageServices();
		IHMMenu.entetesMenu("Livraison de volailles");
		int[] tabNombres = new int[3];
		tabNombres[0] = ihmE.demanderNombreDeVolailles("poulets", elevage.getScanner());
		tabNombres[1] = ihmE.demanderNombreDeVolailles("canards", elevage.getScanner());
		tabNombres[2] = ihmE.demanderNombreDeVolailles("paons", elevage.getScanner());
		if (cv.nbMaxParLivraisonTypeVolaillePasDepasse(tabNombres)) {
			Map<String, Volaille> livraison = ihmE.creationLivraison(tabNombres, elevage.getScanner());
			es.ajoutLivraisonAElevage(livraison, elevage);
		}
		ihmE.confirmationLivraisonEffectuee(tabNombres);			
	}
	
	/**
	 * Permet de modifier le poids d'abattage d'un poulet ou d'un canard
	 * @param elevage : l'elevage dont on veut modifier l'un des poids d'abattage
	 */
	private static void modifierPoidsDAbattage(Elevage elevage) {
		IHMVolailleComestible ihmVC = new IHMVolailleComestible();
		PouletService ps = new PouletService();
		CanardService cs = new CanardService();
		IHMMenu.entetesMenu("Modification poids d'abattage");
		String choix = ihmVC.demndeSaisieChoix(elevage.getScanner());
		if ("poulet".equals(choix) 
				&& ps.modificationPoidsDAbattage(ihmVC.demandeSaisieNouveauPoids(elevage.getScanner()))) {
			modifierAbattableElevage(elevage);
			ihmVC.affichageConfirmationModificationPoids(Poulet.getPoidsFinalKilo(), "d'abattage");
		}
		else if ("canard".equals(choix) 
				&& cs.modificationPoidsDAbattage(ihmVC.demandeSaisieNouveauPoids(elevage.getScanner()))) {
			modifierAbattableElevage(elevage);
			ihmVC.affichageConfirmationModificationPoids(Canard.getPoidsFinalKilo(), "d'abattage");
		}
		else {
			System.out.println(OPERATION_IMPOSSIBLE);
		}
	}
	
	/**
	 * Permet de modifier le statut d'abattable de tout l'elevage 
	 * @param elevage : l'elevage gere
	 */
	private static void modifierAbattableElevage(Elevage elevage) {
		VolailleComestibleServices vcs = new VolailleComestibleServices();
		for (Entry<String, Volaille> volaille : elevage.getListeVolailles().entrySet()) {
			vcs.modificationStatutAbattable((VolailleComestible) volaille.getValue());
		}
	}
	
	/**
	 * Permet de modifier le prix du jour d'un poulet ou d'un canard
	 * @param elevage : l'elevage dont on veut modifier l'un des prix du jour
	 */
	private static void modifierPrix(Elevage elevage) {
		IHMVolailleComestible ihmVC = new IHMVolailleComestible();
		PouletService ps = new PouletService();
		CanardService cs = new CanardService();
		IHMMenu.entetesMenu("Modification prix");
		String choix = ihmVC.demndeSaisieChoix(elevage.getScanner());
		if ("poulet".equals(choix) 
				&& ps.modificationPrix(ihmVC.demandeSaisieNouveauPrix(elevage.getScanner()))) {
			ihmVC.affichageConfirmationModificationDuPrix(Poulet.getPrix(), choix);
			
		}
		else if ("canard".equals(choix) 
				&& cs.modificationPrix(ihmVC.demandeSaisieNouveauPrix(elevage.getScanner()))) {
			ihmVC.affichageConfirmationModificationDuPrix(Canard.getPoidsFinalKilo(), choix);
		}
		else {
			System.out.println(OPERATION_IMPOSSIBLE);
		}
	}
	
	/**
	 * Permet de modifier le poids actuel d'un poulet ou d'un canard
	 * @param elevage : l'elevage ou se trouve la volaille comestible
	 */
	private static void modifierPoids(Elevage elevage) {
		IHMVolaille ihmV = new IHMVolaille();
		IHMVolailleComestible ihmVC = new IHMVolailleComestible();
		VolailleComestibleServices vcs = new VolailleComestibleServices();
		IHMMenu.entetesMenu("Modification poids d'une volaille comestible");
		String idVolaille = ihmV.demanderIdentifiantVolaille(elevage.getScanner());
		float poids = ihmVC.demandeSaisieNouveauPoids(elevage.getScanner());
		if (vcs.modificationPoidsActuel(idVolaille, poids, elevage)) {
			ihmVC.affichageConfirmationModificationPoids(poids, "");
		}
		else {
			System.out.println(OPERATION_IMPOSSIBLE);
		}
	}
	
	/**
	 * Permet de vendre une volaille comestible et d'augemnter le chiffre d'affaire de l'elevage
	 * @param elevage : l'elevage ou se trouve la volaille comestible
	 */
	private static void vendreVolaille(Elevage elevage) {
		IHMVolaille ihmV = new IHMVolaille();
		IHMElevage ihmE = new IHMElevage();
		IHMVolailleComestible ihmVC = new IHMVolailleComestible();
		ElevageServices es = new ElevageServices();
		IHMMenu.entetesMenu("Vente d'une volaille");
		String idVolaille = ihmV.demanderIdentifiantVolaille(elevage.getScanner());
		Volaille volaille = es.rechercheVolailleParId(idVolaille, elevage.getListeVolailles());
		if (es.venteVolailleComestible(idVolaille, elevage)) {
			ihmVC.affichageConfirmationVolailleComestibleVendue(idVolaille);
			if (volaille instanceof Poulet) {
				ihmE.afficheageArgentAjouteEtChiffreDAffaire(Poulet.getPrix(), elevage.getChiffreDAffaire());
			}
			else {
				ihmE.afficheageArgentAjouteEtChiffreDAffaire(Canard.getPrix(), elevage.getChiffreDAffaire());
			}
		}
		else {
			System.out.println(OPERATION_IMPOSSIBLE);
		}
	}
	
	/**
	 * Permet de rendre un paon a un parc
	 * @param elevage : l'elevage ou se trouve le paon
	 */
	private static void rendreUneVolaille(Elevage elevage) {
		IHMVolaille ihmV = new IHMVolaille();
		IHMPaon ihmP = new IHMPaon();
		ElevageServices es = new ElevageServices();
		IHMMenu.entetesMenu("Rendre une volaille a un parc");
		String idVolaille = ihmV.demanderIdentifiantVolaille(elevage.getScanner());
		if (es.rendreVolailleNonComestible(idVolaille, elevage)) {
			ihmP.paonRendu(idVolaille, Paon.ORIGINE);
		}
		else {
			System.out.println(OPERATION_IMPOSSIBLE);
		}
	}
	
	/**
	 * Permet d'afficher le total des prix des volailles abattables
	 * @param elevage : elevage que l'on gere
	 */
	private static void afficherTotalPrixVolaillesAbattables(Elevage elevage) {
		IHMElevage ihmE = new IHMElevage();
		ElevageServices es = new ElevageServices();
		IHMMenu.entetesMenu("Affichage du total des prix des volailles abattables");
		ihmE.affichageTotalPrixVolaillesAbattables(es.calculTotalPrix(elevage.getListeVolailles()));
	}
	
	/**
	 * Permet d'afficher le nombre de volailles de chaque types presentes dans l'elevage
	 * ainsi qu'une representation graphique
	 * @param elevage : elevage que l'on gere
	 */
	private static void afficherNombresDessinVolaillesParType(Elevage elevage) {
		IHMElevage ihmE = new IHMElevage();
		ElevageServices es = new ElevageServices();
		IHMMenu.entetesMenu("Affichage du nombre de volailles par type");
		ihmE.affichageNombreVolaillesParType(elevage.getNom(), es.tableauNombresParTypeVolailles(elevage.getListeVolailles()));
		ihmE.afficherRepresentationGraphiqueVolaillesParType(elevage.getListeVolailles());
	}
	
	/**
	 * Permet de quitter le programme
	 * @param choix : choix de l'utilisateur
	 * @param elevage : elevage que l'on gere
	 */
	private static void quitter(String choix, Elevage elevage) {
		if (!"Q".equals(choix)) {
			menuGestionElevage(elevage);
		}
	}
	
	/**
	 * Permet de lancer le menu
	 * @param elevage
	 */
	private static void menuGestionElevage(Elevage elevage) {
		IHMMenu.affichageMenu();
		String choix = IHMMenu.demanderSaisirChoix(elevage.getScanner());
		switch (choix) {
			case "A" : modifierNomElevage(elevage);
				break;
			case "B" : modifierInfosEleveur(elevage);
				break;
			case "C" : afficherNombresDessinVolaillesParType(elevage);
				break;
			case "D" : afficherTotalPrixVolaillesAbattables(elevage);
				break;
			case "E" : livraisonVolailles(elevage);
				break;
			case "F" : modifierPoidsDAbattage(elevage);
				break;
			case "G" : modifierPrix(elevage);
				break;
			case "H" : modifierPoids(elevage);
				break;
			case "I" : vendreVolaille(elevage);
				break;
			case "J" : rendreUneVolaille(elevage);
				break;
		}
		 quitter(choix, elevage);
	}
	
	/**
	 * Permet de s'authentifier
	 * @param elevage : elevage que l'on gere
	 */
	public static void menuAuthentification(Elevage elevage) {
		IHMEleveur ihmE = new IHMEleveur();
		IHMMenu.entetesMenu("Menu d'authentification");
		ControleEleveur ce = new ControleEleveur();
		String login = ihmE.demanderSaisieLogin(elevage.getScanner());
		String mdp = ihmE.demanderSaisieMDP(elevage.getScanner());
		while (!ce.verificationLoginMdp(elevage.getEleveur(), login, mdp)) {
			System.out.println("Login et/ou mot de passe incorrect(s) !");
			if (IHMGeneral.demanderSaisieConfirmationPoursuite(elevage.getScanner())) {
				menuAuthentification(elevage);
			}
		}
		if (ce.verificationLoginMdp(elevage.getEleveur(), login, mdp)) {
			System.out.println("\n");
			System.out.println("Bienvenue dans l'elevage "+elevage.getNom());
			menuGestionElevage(elevage);
		}
	}
	
}
