package fr.afpa.services;

import fr.afpa.controles.ControleElevage;
import fr.afpa.controles.ControleVolaille;
import fr.afpa.controles.ControleVolailleComestible;
import fr.afpa.entites.Elevage;
import fr.afpa.entites.Volaille;
import fr.afpa.entites.VolailleComestible;

public class VolailleComestibleServices {

	/**
	 * Permet de modifier le poids actuel d'une volaille comestible dans elevage
	 * @param idVolaille : identifiant de la volaille dont on veut modifier le poids
	 * @param poids      : le nouveau poids
	 * @param elevage    : elevage contenant la volaille
	 * @return true si le poids a ete modifie et false sinon
	 */
	public boolean modificationPoidsActuel(String idVolaille, float poids, Elevage elevage) {
		ControleVolaille cv = new ControleVolaille();
		ControleElevage ce = new ControleElevage();
		ElevageServices es = new ElevageServices();
		ControleVolailleComestible cvc = new ControleVolailleComestible();
		Volaille volaille;
		if (cv.identifiantValide(idVolaille) && ce.existenceVolailleParId(idVolaille, elevage.getListeVolailles())) {
			volaille = es.rechercheVolailleParId(idVolaille, elevage.getListeVolailles());
			if (cv.volailleEstComestible(volaille) && cvc.poidsPrixValide(poids)) {
				((VolailleComestible) volaille).setPoids(poids);
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	/**
	 * Permet de modifier le statut d'abattable d'une volaille comestible
	 * @param volailleC : la volaille dont on veut modifier le statut
	 * @return true si le statut a ete modifie et false sinon
	 */
	public boolean modificationStatutAbattable(VolailleComestible volailleC) {
		ControleVolailleComestible cvc = new ControleVolailleComestible();
		if (cvc.volailleChangeStatutAbattable(volailleC)) {
			volailleC.setAbattable(true);
		} 
		else {
			volailleC.setAbattable(false);
		}
		return true;
	}

}
