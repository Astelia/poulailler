package fr.afpa.services;

import fr.afpa.controles.ControleVolailleComestible;
import fr.afpa.entites.Canard;

public class CanardService implements IVolailleComestible {

	/**
	 * Permet de modifier le poids d'abattage des canards
	 * @param poids : le nouveau poids
	 * @return true si la modification a ete effectuee et false sinon
	 */
	@Override
	public boolean modificationPoidsDAbattage(float poids) {
		ControleVolailleComestible cvc = new ControleVolailleComestible();
		if (cvc.poidsPrixValide(poids)) {
			Canard.setPoidsFinalKilo(poids);
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Permet de modifier le prix des canards
	 * @param prix : le nouveau prix
	 * @return true si la modification a ete effectuee et false sinon
	 */
	@Override
	public boolean modificationPrix(float prix) {
		ControleVolailleComestible cvc = new ControleVolailleComestible();
		if (cvc.poidsPrixValide(prix)) {
			Canard.setPrix(prix);
			return true;
		}
		else {
			return false;
		}
	}

}
