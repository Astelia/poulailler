package fr.afpa.services;

import fr.afpa.controles.ControleVolailleComestible;
import fr.afpa.entites.Poulet;

public class PouletService implements IVolailleComestible {

	/**
	 * Permet de modifier le poids d'abattage des poulets
	 * @param poids : le nouveau poids
	 * @return true si la modification a ete effectuee et false sinon
	 */
	@Override
	public boolean modificationPoidsDAbattage(float poids) {
		ControleVolailleComestible cvc = new ControleVolailleComestible();
		if (cvc.poidsPrixValide(poids)) {
			Poulet.setPoidsFinalKilo(poids);
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * Permet de modifier le prix des poulets
	 * @param prix : le nouveau prix
	 * @return true si la modification a ete effectuee et false sinon
	 */
	@Override
	public boolean modificationPrix(float prix) {
		ControleVolailleComestible cvc = new ControleVolailleComestible();
		if (cvc.poidsPrixValide(prix)) {
			Poulet.setPrix(prix);
			return true;
		}
		else {
			return false;
		}
	}

}
