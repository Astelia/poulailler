package fr.afpa.services;

import fr.afpa.entites.Eleveur;

public class EleveurServices {

	/**
	 * Permet de modifier les informations d'un eleveur
	 * @param infos : tableau qui contient les nouveaux nom, prenom, login et mot de passe dans cet ordre
	 * @param eleveur : eleveur a modifier
	 * @return true si la modification a ete effectuee et false sinon
	 */
	public boolean modificationInfosEleveur(String[] infos, Eleveur eleveur) {
		eleveur.setNom(infos[0]);
		eleveur.setPrenom(infos[1]);
		eleveur.setLogin(infos[2]);
		eleveur.setMotDePasse(infos[3]);
		return true;		
	}
	
}
