package fr.afpa.services;

public interface IVolailleComestible {

	/**
	 * Permet de modifier le poids d'abattage d'une volaille comestible
	 * @param poids : le nouveau poids
	 * @return true si la modification a ete effectuee et false sinon
	 */
	public abstract boolean modificationPoidsDAbattage(float poids);
	
	/**
	 * Permet de modifier le prix d'une volaille comestible
	 * @param prix : le nouveau prix
	 * @return true si la modification a ete effectuee et false sinon
	 */
	public abstract boolean modificationPrix(float prix);
	
}
