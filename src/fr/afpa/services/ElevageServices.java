package fr.afpa.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import fr.afpa.controles.ControleElevage;
import fr.afpa.controles.ControleVolaille;
import fr.afpa.entites.Canard;
import fr.afpa.entites.Elevage;
import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;
import fr.afpa.entites.VolailleComestible;

public class ElevageServices {

	/**
	 * Permet de calculer le total de prix des volailles abattables
	 * @param elevage : l'elevage dont on souhaite connaitre le prix
	 * @return le total de prix de volailles abattables
	 */
	public float calculTotalPrix(Map<String, Volaille> listeVolailles) {
		float totalPrix = 0;
		for (Entry<String, Volaille> mapVolailles : listeVolailles.entrySet()) {
			Volaille volaille = mapVolailles.getValue();
			if ((volaille instanceof Poulet) && ((Poulet) volaille).isAbattable()) {
				Poulet poulet = (Poulet) volaille;
				totalPrix += Poulet.getPrix() * poulet.getPoids();
			}
			else if ((volaille instanceof Canard) && ((Canard) volaille).isAbattable()) {
				Canard canard = (Canard) volaille;
				totalPrix += Canard.getPrix() * canard.getPoids();
			}
		}
		return totalPrix;
	}
	
	/**
	 * Permet de compter le nombre de chaque type de volailles
	 * @param listeVolailles : liste de volailles qu'on souhaite compter
	 * @return un tableau de nombres de volailles, une case par type de volaille
	 */
	public int[] tableauNombresParTypeVolailles(Map<String, Volaille> listeVolailles) {
		int nbPoulets = 0;
		int nbCanards = 0;
		int nbPaons = 0;
		for (Entry<String, Volaille> mapVolailles : listeVolailles.entrySet()) {
			Volaille volaille = mapVolailles.getValue();
			if (volaille instanceof Poulet) {
				nbPoulets++;
			}
			else if (volaille instanceof Canard) {
				nbCanards++;
			}
			else {
				nbPaons++;
			}
		}
		int[] res = {nbPoulets, nbCanards, nbPaons};
		return res;
	}
	
	/**
	 * Permet de faire une representation graphique d'une liste de volailles
	 * @param listeVolailles : liste de volailles geree
	 * @return : la representation graphique de la liste de volailles
	 */
	public ArrayList<ArrayList<String>> representationGraphiqueElevage(Map<String, Volaille> listeVolailles) {
		ArrayList<ArrayList<String>> dessin = new ArrayList<ArrayList<String>>(3);
		
		listeVolailles = triListeVolailles(listeVolailles);
		
		ArrayList<String> listePoulets = new ArrayList<String>();
		ArrayList<String> listeCanards = new ArrayList<String>();
		ArrayList<String> listePaons = new ArrayList<String>();
		for (Entry<String, Volaille> volaille : listeVolailles.entrySet()) {
			if (volaille.getValue() instanceof Poulet) {
				Poulet poulet = (Poulet) volaille.getValue();
				if (poulet.isAbattable()) {
					listePoulets.add("PP ");
				}
				else {
					listePoulets.add("P  ");
				}
			}
			else if (volaille.getValue() instanceof Canard) {
				Canard canard = (Canard) volaille.getValue();
				if (canard.isAbattable()) {
					listeCanards.add("CC ");
				}
				else {
					listeCanards.add("C  ");
				}
			}
			else {
				listePaons.add("Pa ");
			}
		}
		dessin.add(listePoulets);
		dessin.add(listeCanards);
		dessin.add(listePaons);
		return dessin;
	}
	
	/**
	 * Permet de trier une liste de volailles en mettant toutes les volailles abattables avant les autres
	 * @param listeVolailles : liste a trier
	 * @return la liste de volailles triee
	 */
	private Map<String, Volaille> triListeVolailles(Map<String, Volaille> listeVolailles) {
		Map<String, Volaille> res = new HashMap<String, Volaille>();
		for (Entry<String, Volaille> volaille : listeVolailles.entrySet()) {
			if (volaille.getValue() instanceof Poulet || volaille.getValue() instanceof Canard) {
				VolailleComestible volailleComestible = (VolailleComestible) volaille.getValue();
				if (volailleComestible.isAbattable()) {
					res.put(volaille.getKey(), volaille.getValue());
				}
			}
		}
		for (Entry<String, Volaille> volaille : listeVolailles.entrySet()) {
			if (!res.containsKey(volaille.getKey())) {
				res.put(volaille.getKey(), volaille.getValue());
			}
		}
		return res;
	}
	
	/**
	 * Permet de modifier le nom d'un elevage
	 * @param nom : le nouveau nom
	 * @param elevage : l'elevage dont on souhaite modifier le nom
	 * @return true pour une operation qui s'est bien passee
	 */
	public boolean modificationNomDElevage(String nom, Elevage elevage) {
		elevage.setNom(nom);
		return true;
	}
	
	/**
	 * Permet de rechercher une volaille par son identifiant dans une liste de volailles
	 * @param idVolaille : identifiant de la volaille
	 * @param listeVolailles : liste de volailles dans laquelle on cherche notre volaille
	 * @return la volaille trouvee ou un null
	 */
	public Volaille rechercheVolailleParId(String idVolaille, Map<String, Volaille> listeVolailles) {
		return listeVolailles.get(idVolaille);
	}
	
	/**
	 * Permet d'ajouter le prix d'une volaille comestible au chiffre d'affaire d'un elevage
	 * @param volailleC : volaille qui fournit le montant
	 * @param elevage : elevage qui possede le chiffre d'affaire a modifier
	 * @return true pour une operation qui s'est bien passee
	 */
	public boolean ajoutArgentAChiffreDAffaire(VolailleComestible volailleC, Elevage elevage) {
		if (volailleC instanceof Poulet) {
			elevage.setChiffreDAffaire(elevage.getChiffreDAffaire() + Poulet.getPrix());
		}
		else {
			elevage.setChiffreDAffaire(elevage.getChiffreDAffaire() + Canard.getPrix());
		}
		return true;
	}
	
	/**
	 * Permet de vendre une volaille comestible
	 * @param idVolaille : id de la volaille a vendre
	 * @param elevage : elevage qui possede la volaille
	 * @return true si la volaille a ete vendue et false sinon
	 */
	public boolean venteVolailleComestible(String idVolaille, Elevage elevage) {
		ControleVolaille cv = new ControleVolaille();
		ControleElevage ce = new ControleElevage();
		Volaille volaille;
		if (cv.identifiantValide(idVolaille) && ce.existenceVolailleParId(idVolaille, elevage.getListeVolailles())) {
			volaille = rechercheVolailleParId(idVolaille, elevage.getListeVolailles());
			if (cv.volailleEstComestible(volaille) && ((VolailleComestible) volaille).isAbattable()) {
				ajoutArgentAChiffreDAffaire((VolailleComestible) volaille, elevage);
				elevage.getListeVolailles().remove(idVolaille);
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	/**
	 * Permet d'ajouter une livraison a une liste de volailles
	 * @param livraison : la livraison a ajouter
	 * @param listeVolailles : la liste de volailles qui recoit la livraison
	 * @return true pour une operation qui s'est bien passee
	 */
	public boolean ajoutLivraisonAElevage(Map<String, Volaille> livraison, Elevage elevage) {
		elevage.getListeVolailles().putAll(livraison);
		return true;
	}
	
	
	
	/**
	 * Permet de rendre une volaille non comestible a son parc d'origine
	 * @param idVolaille : l'id de la volaille non comestible a rendre
	 * @param elevage : l'elevage qui contient la volaille
	 * @return true si la volaille a ete rendue et false sinon
	 */
	public boolean rendreVolailleNonComestible(String idVolaille, Elevage elevage) {
		ControleVolaille cv = new ControleVolaille();
		ControleElevage ce = new ControleElevage();
		Volaille volaille;
		if (cv.identifiantValide(idVolaille) && ce.existenceVolailleParId(idVolaille, elevage.getListeVolailles())) {
			volaille = rechercheVolailleParId(idVolaille, elevage.getListeVolailles());
			if (!cv.volailleEstComestible(volaille)) {
				elevage.getListeVolailles().remove(idVolaille);
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
}
