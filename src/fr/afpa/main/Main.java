package fr.afpa.main;

import java.util.Scanner;

import fr.afpa.entites.Elevage;
import fr.afpa.services.MenuServices;

public class Main {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		Elevage elevage = new Elevage("Le Gaulois", scanner);
		MenuServices.menuAuthentification(elevage);
		scanner.close();
	}

}
