package fr.afpa.tests.entites;

import org.junit.jupiter.api.Test;

import fr.afpa.entites.Poulet;
import junit.framework.TestCase;

public class TestSetPoulet extends TestCase {

	private Poulet poulet;
	
	public void setUp() throws Exception {
		super.setUp();
		poulet = new Poulet();
	}

	@Test
	public void testSetAbattable() {
		boolean abattable = true;
		assertFalse("Probleme initialisation abattable", poulet.isAbattable());
		poulet.setAbattable(abattable);
		assertTrue("Probleme setAbattable", poulet.isAbattable());
	}
	
	@Test
	public void testSetPoidsActuel() {
		float poids = 3;
		assertEquals("Probleme initialisation poids actuel", 0f, poulet.getPoids());
		poulet.setPoids(poids);
		assertEquals("Probleme setPoids", poids, poulet.getPoids());
	}
	
	@Test
	public void testSetPoidsDAbattage() {
		float poids = 10;
		Poulet.setPoidsFinalKilo(0);
		assertEquals("Probleme initialisation poids d'abattage ", 0f, Poulet.getPoidsFinalKilo());
		Poulet.setPoidsFinalKilo(poids);
		assertEquals("Probleme setPoidsFinalKilo", poids, Poulet.getPoidsFinalKilo());
	}
	
	@Test
	public void testSetPrix() {
		float prix = 3;
		Poulet.setPrix(0);
		assertEquals("Probleme initialisation prix", 0f, Poulet.getPrix());
		Poulet.setPrix(prix);
		assertEquals("Probleme setPrix", prix, Poulet.getPrix());
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		poulet = null;
	}

}
