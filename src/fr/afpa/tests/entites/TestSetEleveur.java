package fr.afpa.tests.entites;

import org.junit.jupiter.api.Test;

import fr.afpa.entites.Eleveur;
import junit.framework.TestCase;

public class TestSetEleveur extends TestCase {

	private Eleveur eleveur;
	
	public void setUp() throws Exception {
		super.setUp();
		eleveur = new Eleveur();
	}

	
	@Test
	public void testSetNom() {
		String nom = "LeGaulois";
		assertEquals("Probleme initialisation", "nom", eleveur.getNom());
		eleveur.setNom(nom);
		assertEquals("Probleme setNom", nom, eleveur.getNom());
	}
	
	@Test
	public void testSetPrenom() {
		String prenom = "Asterix";
		assertEquals("Probleme initialisation", "prenom", eleveur.getPrenom());
		eleveur.setPrenom(prenom);
		assertEquals("Probleme setPrenom", prenom, eleveur.getPrenom());
	}
	
	@Test
	public void testSetLogin() {
		String login = "idefix";
		assertEquals("Probleme initialisation", "admin", eleveur.getLogin());
		eleveur.setLogin(login);
		assertEquals("Probleme setLogin", login, eleveur.getLogin());
	}
	
	@Test
	public void testSetMotDePasse() {
		String mdp = "potionmagique";
		assertEquals("Probleme initialisation", "admin", eleveur.getMotDePasse());
		eleveur.setMotDePasse(mdp);
		assertEquals("Probleme setMotDePasse", mdp, eleveur.getMotDePasse());
	}
	
	
	public void tearDown() throws Exception {
		super.tearDown();
		eleveur = null;
	}

	

}
