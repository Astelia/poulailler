package fr.afpa.tests.entites;

import java.util.Scanner;

import org.junit.jupiter.api.Test;

import fr.afpa.entites.Elevage;
import junit.framework.TestCase;

public class TestElevage extends TestCase {

	private Elevage elevage;
	
	@Test
	public void testCreationElevage() {
		String nom = "KFC";
		Scanner scanner = new Scanner("test");
		elevage = new Elevage(nom, scanner);
		assertEquals("Probleme instanciation nom", nom, elevage.getNom());
		assertSame("Probleme instanciation scanner", scanner, elevage.getScanner());
		
		assertNotNull("Probleme instanciation listeVolailles", elevage.getListeVolailles());
		assertTrue("Probleme instanciation listevolailles", elevage.getListeVolailles().isEmpty());
		
		assertNotNull("Probleme instanciation eleveur", elevage.getEleveur());
		assertEquals("Probleme instanciation chiffre d'affaire", 0f, elevage.getChiffreDAffaire());
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		elevage = null;
	}

}
