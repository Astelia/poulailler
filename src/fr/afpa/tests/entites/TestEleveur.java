package fr.afpa.tests.entites;

import org.junit.jupiter.api.Test;

import fr.afpa.entites.Eleveur;
import junit.framework.TestCase;

public class TestEleveur extends TestCase {

	private Eleveur eleveur; 
	
	@Test
	public void testCreationEleveur() {
		eleveur = new Eleveur();
		assertEquals("Probleme instanciation nom", "nom", eleveur.getNom());
		assertEquals("Probleme instanciation prenom", "prenom", eleveur.getPrenom());
		assertEquals("Probleme instanciation login", "admin", eleveur.getLogin());
		assertEquals("Probleme instanciation mot de passe", "admin", eleveur.getMotDePasse());
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		eleveur = null;
	}

	

}
