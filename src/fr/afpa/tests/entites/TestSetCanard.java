package fr.afpa.tests.entites;

import org.junit.jupiter.api.Test;

import fr.afpa.entites.Canard;
import junit.framework.TestCase;

public class TestSetCanard extends TestCase {

	private Canard canard;
	
	public void setUp() throws Exception {
		super.setUp();
		canard = new Canard();
	}

	@Test
	public void testSetAbattable() {
		boolean abattable = true;
		assertFalse("Probleme initialisation abattable", canard.isAbattable());
		canard.setAbattable(abattable);
		assertTrue("Probleme setAbattable", canard.isAbattable());
	}
	
	@Test
	public void testSetPoidsActuel() {
		float poids = 3;
		assertEquals("Probleme initialisation poids actuel", 0f, canard.getPoids());
		canard.setPoids(poids);
		assertEquals("Probleme setPoids", poids, canard.getPoids());
	}
	
	@Test
	public void testSetPoidsDAbattage() {
		float poids = 10;
		Canard.setPoidsFinalKilo(0);
		assertEquals("Probleme initialisation poids d'abattage", 0f, Canard.getPoidsFinalKilo());
		Canard.setPoidsFinalKilo(poids);
		assertEquals("Probleme setPoidsFinalKilo", poids, Canard.getPoidsFinalKilo());
	}
	
	@Test
	public void testSetPrix() {
		float prix = 3;
		Canard.setPrix(0);
		assertEquals("Probleme initialisation prix", 0f, Canard.getPrix());
		Canard.setPrix(prix);
		assertEquals("Probleme setPrix", prix, Canard.getPrix());
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		canard = null;
	}

}
