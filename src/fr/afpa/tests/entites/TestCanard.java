package fr.afpa.tests.entites;

import org.junit.jupiter.api.Test;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Volaille;
import junit.framework.TestCase;

public class TestCanard extends TestCase {

	private Canard canard;
	
	@Test
	public void testCreationCanard() {
		Volaille.setCpt(0);
		Canard.setPoidsFinalKilo(5);
		float poidsNonDepassant = 3f;
		canard = new Canard(poidsNonDepassant);
		assertEquals("Probleme initialisation poids", poidsNonDepassant, canard.getPoids());
		assertFalse("Probleme intialisation abattable", canard.isAbattable());
		
		float poidsDepassant = 10f;
		canard = new Canard(poidsDepassant);
		assertTrue("Probleme intialisation abattable", canard.isAbattable());
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		canard = null;
	}

}
