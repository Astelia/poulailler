package fr.afpa.tests.entites;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Elevage;
import fr.afpa.entites.Eleveur;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;
import junit.framework.TestCase;

public class TestSetElevage extends TestCase {

	private Elevage elevage;
	private Volaille paon;
	private Volaille poulet;
	private Volaille canard;
	private Map<String, Volaille> listeVolailles;
	
	
	public void setUp() throws Exception {
		super.setUp();
		Volaille.setCpt(0);
		elevage = new Elevage();
		paon = new Paon();
		poulet = new Poulet();
		canard = new Canard();
		
		listeVolailles = new HashMap<String, Volaille>();
		listeVolailles.put(paon.getNumeroIdentification(), paon);
		listeVolailles.put(poulet.getNumeroIdentification(), poulet);
		listeVolailles.put(canard.getNumeroIdentification(), canard);
	}

	@Test
	public void testSetNom() {
		String nom = "KFC";
		assertEquals("Probleme initialisation nom", "Le Gaulois", elevage.getNom());
		elevage.setNom(nom);
		assertEquals("Probleme setNom", nom, elevage.getNom());
	}
	
	@Test
	public void testSetScanner() {
		Scanner scanner = new Scanner("Test");
		assertNull("Probleme initialisation scanner", elevage.getScanner());
		elevage.setScanner(scanner);
		assertSame("Probleme setScanner", scanner, elevage.getScanner());
	}
	
	@Test
	public void testSetEleveur() {
		Eleveur eleveur = new Eleveur("toto", "titi", "tata", "tutu");
		assertEquals("Probleme initialisation eleveur", new Eleveur(), elevage.getEleveur());
		elevage.setEleveur(eleveur);
		assertEquals("Probleme setEleveur", eleveur, elevage.getEleveur());
	}
	
	@Test
	public void testSetChiffreDAffaire() {
		float montant = 3600.80f;
		assertEquals("Probleme initialisation chiffre d'affaire", 0f, elevage.getChiffreDAffaire());
		elevage.setChiffreDAffaire(montant);
		assertEquals("Probleme setChiffreDAffaire", montant, elevage.getChiffreDAffaire());
	}
	
	@Test
	public void testSetListeVolailles() {
		assertNotNull("Probleme initialisation liste de volailles", elevage.getListeVolailles());
		assertTrue("Probleme initialisation liste de volailles", elevage.getListeVolailles().isEmpty());
		elevage.setListeVolailles(listeVolailles);
		assertEquals("Probleme setListeVolailles", listeVolailles, elevage.getListeVolailles());
	}
	
	
	public void tearDown() throws Exception {
		super.tearDown();
		elevage = null;
		paon = null;
		poulet = null;
		canard = null;
		listeVolailles = null;
	}

	

}
