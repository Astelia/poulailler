package fr.afpa.tests.entites;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import fr.afpa.entites.Paon;
import fr.afpa.entites.Volaille;
import junit.framework.TestCase;

public class TestPaon extends TestCase {

	private Volaille paon1;
	private Volaille paon2;
	
	
	public void setUp() throws Exception {
		super.setUp();
		Volaille.setCpt(0);
		paon1 = new Paon();
		paon2 = new Paon();
	}

	@Test
	public void testAutoIncrementIdentifiant() {
		assertNotEquals("Probleme d'autoincrementation", paon1.getNumeroIdentification(), paon2.getNumeroIdentification());
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		paon1 = null;
		paon2 = null;
	}

	

}
