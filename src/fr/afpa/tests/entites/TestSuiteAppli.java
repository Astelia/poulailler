package fr.afpa.tests.entites;

import org.junit.jupiter.api.Test;

import fr.afpa.tests.controles.*;
import fr.afpa.tests.services.*;
import junit.extensions.ActiveTestSuite;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class TestSuiteAppli extends TestCase {

	public TestSuiteAppli(String name) {
		super(name);
	}

	
	@Test
	public static TestSuite suite() {
		TestSuite suite = new ActiveTestSuite();
		suite.addTest(new TestSuite(TestEleveur.class));
		suite.addTest(new TestSuite(TestSetEleveur.class));
		suite.addTest(new TestSuite(TestPaon.class));
		suite.addTest(new TestSuite(TestPoulet.class));
		suite.addTest(new TestSuite(TestCanard.class));
		suite.addTest(new TestSuite(TestSetPoulet.class));
		suite.addTest(new TestSuite(TestSetCanard.class));
		suite.addTest(new TestSuite(TestElevage.class));
		suite.addTest(new TestSuite(TestSetElevage.class));
		suite.addTest(new TestSuite(TestControleSaisieGenerale.class));
		suite.addTest(new TestSuite(TestControleElevage.class));
		suite.addTest(new TestSuite(TestControleEleveur.class));
		suite.addTest(new TestSuite(TestControleVolaille.class));
		suite.addTest(new TestSuite(TestControleVolailleComestible.class));
		suite.addTest(new TestSuite(TestVolailleComestibleServices.class));
		suite.addTest(new TestSuite(TestEleveurService.class));
		suite.addTest(new TestSuite(TestPouletService.class));
		suite.addTest(new TestSuite(TestCanardService.class));
		suite.addTest(new TestSuite(TestElevageService.class));
		return suite;
	}
	
	
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

}
