package fr.afpa.tests.entites;

import org.junit.jupiter.api.Test;

import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;
import junit.framework.TestCase;

public class TestPoulet extends TestCase {

	private Poulet poulet;
	
	@Test
	public void testCreationPoulet() {
		Volaille.setCpt(0);
		Poulet.setPoidsFinalKilo(5);
		Float poidsNonDepassant = new Float(3);
		poulet = new Poulet(poidsNonDepassant);
		assertEquals("Probleme initialisation poids", poidsNonDepassant, poulet.getPoids());
		assertFalse("Probleme intialisation abattable", poulet.isAbattable());
		
		Float poidsDepassant = new Float(10f);
		poulet = new Poulet(poidsDepassant);
		assertTrue("Probleme intialisation abattable", poulet.isAbattable());
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		poulet = null;
	}

}
