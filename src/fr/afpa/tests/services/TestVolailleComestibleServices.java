package fr.afpa.tests.services;

import org.junit.jupiter.api.Test;

import fr.afpa.entites.Elevage;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;
import fr.afpa.services.VolailleComestibleServices;
import junit.framework.TestCase;

public class TestVolailleComestibleServices extends TestCase {

	private VolailleComestibleServices vcs;
	private Poulet poulet;
	private Paon paon;
	private float poids;
	private Elevage elevage;
	
	
	public void setUp() throws Exception {
		super.setUp();
		Volaille.setCpt(0);
		vcs = new VolailleComestibleServices();
		Poulet.setPoidsFinalKilo(10);
		poulet = new Poulet();
		paon = new Paon();
		poids = 5;
		elevage = new Elevage();
		elevage.getListeVolailles().put(poulet.getNumeroIdentification(), poulet);
		elevage.getListeVolailles().put(paon.getNumeroIdentification(), paon);
	}

	@Test
	public void testModificationPoidsActuelIdentifiantInvalide() {
		assertFalse("Probleme modification poids actuel", vcs.modificationPoidsActuel("truc", poids, elevage));
	}
	
	@Test
	public void testModificationPoidsActuelSiVolailleNExistePas() {
		assertFalse("Probleme modification poids actuel", vcs.modificationPoidsActuel("0000456", poids, elevage));
	}
	
	@Test
	public void testModificationPoidsActuelVolailleNonComestible() {
		assertFalse("Probleme modification poids actuel", vcs.modificationPoidsActuel(paon.getNumeroIdentification(), poids, elevage));
	}
	
	@Test
	public void testModificationPoidsActuelPoidsNegatif() {
		assertFalse("Probleme modification poids actuel", vcs.modificationPoidsActuel(poulet.getNumeroIdentification(), -4, elevage));
	}
	
	@Test
	public void testModificationPoidsActuelCorrect() {
		assertTrue("Probleme modification poids actuel", vcs.modificationPoidsActuel(poulet.getNumeroIdentification(), poids, elevage));
	}
	
	@Test
	public void testChangementStatutAbattable() {
		assertTrue("Probleme modification statut abattable", vcs.modificationStatutAbattable(poulet));
		poulet.setPoids(11);
		assertTrue("Probleme modification statut abattable", vcs.modificationStatutAbattable(poulet));
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		vcs = null;
		poulet = null;
		paon = null;
		elevage = null;
	}


}
