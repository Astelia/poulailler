package fr.afpa.tests.services;

import org.junit.jupiter.api.Test;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Volaille;
import fr.afpa.services.CanardService;
import junit.framework.TestCase;

public class TestCanardService extends TestCase {

	private CanardService cs;
	
	public void setUp() throws Exception {
		super.setUp();
		Volaille.setCpt(0);
		cs = new CanardService();
		Canard.setPoidsFinalKilo(0);
		Canard.setPrix(0);
	}

	@Test
	public void testModificationPoidsAbattageValide() {
		assertTrue("Probleme modification poids abattage", cs.modificationPoidsDAbattage(20));
	}
	
	@Test
	public void testModificationPoidsAbattageInvalide() {
		assertFalse("Probleme modification poids abattage", cs.modificationPoidsDAbattage(-3));
	}
	
	@Test
	public void testModificationPrixValide() {
		assertTrue("Probleme modification prix", cs.modificationPrix(24));
	}
	
	@Test
	public void testModificationPrixInvalide() {
		assertFalse("Probleme modification prix", cs.modificationPrix(-2));
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		cs = null;
	}

}
