package fr.afpa.tests.services;

import org.junit.jupiter.api.Test;

import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;
import fr.afpa.services.PouletService;
import junit.framework.TestCase;

public class TestPouletService extends TestCase {

	private PouletService ps;
	
	public void setUp() throws Exception {
		super.setUp();
		Volaille.setCpt(0);
		ps = new PouletService();
		Poulet.setPoidsFinalKilo(0);
		Poulet.setPrix(0);
	}

	@Test
	public void testModificationPoidsAbattageValide() {
		assertTrue("Probleme modification poids abattage", ps.modificationPoidsDAbattage(20));
	}
	
	@Test
	public void testModificationPoidsAbattageInvalide() {
		assertFalse("Probleme modification poids abattage", ps.modificationPoidsDAbattage(-3));
	}
	
	@Test
	public void testModificationPrixValide() {
		assertTrue("Probleme modification prix", ps.modificationPrix(24));
	}
	
	@Test
	public void testModificationPrixInvalide() {
		assertFalse("Probleme modification prix", ps.modificationPrix(-2));
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		ps = null;
	}

	
}
