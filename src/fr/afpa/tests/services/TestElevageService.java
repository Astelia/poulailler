package fr.afpa.tests.services;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Elevage;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;
import fr.afpa.entites.VolailleComestible;
import fr.afpa.services.ElevageServices;
import junit.framework.TestCase;

public class TestElevageService extends TestCase {

	private ElevageServices es;
	private Elevage elevage;
	private int[] tabNombres;
	private Volaille poulet;
	private Volaille canard;
	private Paon paon;
	private float poidsPoulet;
	private float poidsCanard;
	
	@BeforeEach
	public void setUp() throws Exception {
		super.setUp();
		Volaille.setCpt(0);
		Poulet.setPoidsFinalKilo(10);
		es = new ElevageServices();
		elevage = new Elevage();
		tabNombres = new int[3];
		tabNombres[0] = 5;
		tabNombres[1] = 3;
		tabNombres[2] = 1;
		Map<String, Volaille> listeVolailles = creationLivraison(tabNombres);
		elevage.setListeVolailles(listeVolailles);
		
		poidsPoulet = 12f;
		poidsCanard = 7f;
		poulet = new Poulet(poidsPoulet);
		canard = new Canard(poidsCanard);
		paon= new Paon();
		elevage.getListeVolailles().put(poulet.getNumeroIdentification(), poulet);
		elevage.getListeVolailles().put(canard.getNumeroIdentification(), canard);
		elevage.getListeVolailles().put(paon.getNumeroIdentification(), paon);
	}
	
	public Map<String, Volaille> creationLivraison(int[] tab) {
		Map<String, Volaille> listeVolailles = new HashMap<String, Volaille>();
		for (int i=1; i<=tab[0]; i++) {
			Volaille volaille = new Poulet();
			listeVolailles.put(volaille.getNumeroIdentification(), volaille);
		}
		for (int i=1; i<=tab[1]; i++) {
			Volaille volaille = new Canard();
			listeVolailles.put(volaille.getNumeroIdentification(), volaille);
		}
		for (int i=1; i<=tab[2]; i++) {
			Volaille volaille = new Paon();
			listeVolailles.put(volaille.getNumeroIdentification(), volaille);
		}
		return listeVolailles;
	}

	@Test
	public void testModificationNomElevage() {
		assertTrue("Probleme modification nom elevage", es.modificationNomDElevage("bidule", elevage));
	}
	
	@Test
	public void testCalculNombreVolaillesParType() {
		assertEquals("Probleme tableau nombre volailles par type", tabNombres[0] + 1
							, es.tableauNombresParTypeVolailles(elevage.getListeVolailles())[0]);
		assertEquals("Probleme tableau nombre volailles par type", tabNombres[1] + 1
							, es.tableauNombresParTypeVolailles(elevage.getListeVolailles())[1]);
		assertEquals("Probleme tableau nombre volailles par type", tabNombres[2] + 1
							, es.tableauNombresParTypeVolailles(elevage.getListeVolailles())[2]);
	}
	
	@Test
	public void testCaculTotalPrixVolaillesabattables() {
		assertEquals("Probleme calcul total prix volailles abattables"
				, Poulet.getPrix()*poidsPoulet + Canard.getPrix()*poidsCanard, es.calculTotalPrix(elevage.getListeVolailles()));
	}
	
	@Test
	public void testRechercheVolailleDansElevagePresente() {
		assertNotNull("Probleme recherche volaille", es.rechercheVolailleParId("0000001", elevage.getListeVolailles()));
	}
	
	@Test
	public void testRechercheVolaillePasDansElevage() {
		assertNull("Probleme recherche volaille", es.rechercheVolailleParId("1000100", elevage.getListeVolailles()));
	}
	
	@Test
	public void testVenteVolailleIdentifiantInvalide() {
		assertFalse("Probleme vente volaille", es.venteVolailleComestible("truc", elevage));
	}
	
	@Test
	public void testVenteVolailleNExistePas() {
		assertFalse("Probleme vente volaille", es.venteVolailleComestible("1000100", elevage));
	}
	
	@Test
	public void testVenteVolailleNonComestible() {
		assertFalse("Probleme vente volaille", es.venteVolailleComestible("0000009", elevage));
	}
	
	@Test
	public void testVenteVolaillePasAbattable() {
		assertFalse("Probleme vente volaille", es.venteVolailleComestible("0000001", elevage));
	}
	
	@Test
	public void testVentePouletExistenteComestibleAbattable() {
		assertTrue("Probleme vente volaille", es.venteVolailleComestible(poulet.getNumeroIdentification(), elevage));
	}
	
	@Test
	public void testVenteCanardtExistenteComestibleAbattable() {
		assertTrue("Probleme vente volaille", es.venteVolailleComestible(poulet.getNumeroIdentification(), elevage));
	}
	
	@Test
	public void testAjoutArgentChiffreDAffairePoulet() {
		assertTrue("Probleme ajout d'argent", es.ajoutArgentAChiffreDAffaire((VolailleComestible) poulet, elevage));
	}
	
	@Test
	public void testAjoutArgentChiffreDAffaireCanard() {
		assertTrue("Probleme ajout d'argent", es.ajoutArgentAChiffreDAffaire((VolailleComestible) canard, elevage));
	}
	
	@Test
	public void testRendreVolailleIdentifiantInvalide() {
		assertFalse("Probleme rendre volaille", es.rendreVolailleNonComestible("truc", elevage));
	}
	
	@Test
	public void testRendreVolailleNExistePas() {
		assertFalse("Probleme rendre volaille", es.rendreVolailleNonComestible("1000100", elevage));
	}
	
	@Test
	public void testRendreVolailleComestibleAParc() {
		assertFalse("Probleme rendre volaille", es.rendreVolailleNonComestible(poulet.getNumeroIdentification(), elevage));
	}
	
	@Test
	public void testRendreVolailleNonComestibleAParc() {
		 
		elevage.getListeVolailles().put(paon.getNumeroIdentification(), paon);
		assertTrue("Probleme rendre volaille", es.rendreVolailleNonComestible(paon.getNumeroIdentification(), elevage));
	}
	
	@Test
	public void testAjoutLivraisonDansElevage() {
		Map<String, Volaille> livraison = new HashMap<String, Volaille>();
		livraison.put(canard.getNumeroIdentification(), canard);
		assertTrue("Probleme livraison volailles", es.ajoutLivraisonAElevage(livraison, elevage));
	}
	
	@AfterEach
	public void tearDown() throws Exception {
		super.tearDown();
		es = null;
		elevage = null;
		tabNombres = null;
		poulet = null;
		canard = null;
		paon = null;
	}

}
