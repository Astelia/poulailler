package fr.afpa.tests.services;

import org.junit.jupiter.api.Test;

import fr.afpa.entites.Eleveur;
import fr.afpa.services.EleveurServices;
import junit.framework.TestCase;

public class TestEleveurService extends TestCase {

	private Eleveur eleveur;
	private String[] modifs;
	private EleveurServices es;
	
	public void setUp() throws Exception {
		super.setUp();
		es = new EleveurServices();
		eleveur = new Eleveur();
		modifs = new String[4];
		modifs[0] = "truc";
		modifs[1] = "bidule";
		modifs[2] = "machin";
		modifs[3] = "chouette";
	}

	@Test
	public void testModificationsInformations() {
		assertTrue("Probleme modification eleveur", es.modificationInfosEleveur(modifs, eleveur));
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		eleveur = null;
		modifs = null;
		es = null;
	}

	

}
