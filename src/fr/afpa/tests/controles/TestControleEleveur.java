package fr.afpa.tests.controles;

import org.junit.jupiter.api.Test;

import fr.afpa.controles.ControleEleveur;
import fr.afpa.entites.Eleveur;
import junit.framework.TestCase;

public class TestControleEleveur extends TestCase {

	private ControleEleveur ce;
	private Eleveur eleveur;
	private String login;
	private String mdp;
	
	public void setUp() throws Exception {
		super.setUp();
		ce = new ControleEleveur();
		login = "Disney";
		mdp = "NoirEtBlanc";
		eleveur = new Eleveur("Clarabelle", "Vache", login, mdp);
	}

	
	@Test
	public void testLoginMDPCorrect() {
		assertTrue("Probleme controle login et mdp", ce.verificationLoginMdp(eleveur, login, mdp));
	}
	
	@Test
	public void testLoginIncorrect() {
		assertFalse("Probleme controle login et mdp", ce.verificationLoginMdp(eleveur, "chouette", mdp));
	}
	
	@Test
	public void testMDPIncorrect() {
		assertFalse("Probleme controle login et mdp", ce.verificationLoginMdp(eleveur, login, "machin"));
	}
	
	@Test
	public void testLoginMDPIncorrect() {
		assertFalse("Probleme controle login et mdp", ce.verificationLoginMdp(eleveur, "bidule", "truc"));
	}
	
	
	public void tearDown() throws Exception {
		super.tearDown();
		ce = null;
		login = null;
		mdp = null;
		eleveur = null;
	}

}
