package fr.afpa.tests.controles;

import org.junit.jupiter.api.Test;

import fr.afpa.controles.ControleVolaille;
import fr.afpa.entites.Canard;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;
import junit.framework.TestCase;

public class TestControleVolaille extends TestCase {

	private ControleVolaille cv;
	private Volaille poulet;
	private Volaille canard;
	private Volaille paon;
	
	
	public void setUp() throws Exception {
		super.setUp();
		Volaille.setCpt(0);
		cv = new ControleVolaille();
		poulet = new Poulet();
		canard = new Canard();
		paon = new Paon();
	}

	
	@Test
	public void testIdentifiantValide() {
		assertTrue("Probleme controle identifiant valide", cv.identifiantValide("0000001"));
		assertTrue("Probleme controle identifiant valide"+poulet.getNumeroIdentification(), cv.identifiantValide(poulet.getNumeroIdentification()));
	}
	
	@Test
	public void testIdentifiantNonValide() {
		assertFalse("Probleme controle identifiant valide longueur", cv.identifiantValide("01234"));
		assertFalse("Probleme controle identifiant valide caracteres", cv.identifiantValide("01_34"));
	}
	
	@Test
	public void testVolailleComestibleOuNon() {
		assertTrue("Probleme controle comestible", cv.volailleEstComestible(canard));
		assertTrue("Probleme controle comestible", cv.volailleEstComestible(poulet));
		assertFalse("Probleme controle comestible", cv.volailleEstComestible(paon));
	}
	
	@Test
	public void testNBMaxVolaillesPasDepasse() {
		int[] tab = {1, 3, 3};
		assertTrue("Probleme controle nb max de volailles", cv.nbMaxParLivraisonTypeVolaillePasDepasse(tab));
	}
	
	@Test
	public void testNbMaxVolaillesDepasse() {
		int[] tab = {6, 5, 4};
		assertFalse("Probleme controle nb max de volailles", cv.nbMaxParLivraisonTypeVolaillePasDepasse(tab));
	}
	
	@Test
	public void testNBMaxVolaillesMauvaiseLongueurTableau() {
		int[] tab = {};
		assertFalse("Probleme controle nb max de poulets", cv.nbMaxParLivraisonTypeVolaillePasDepasse(tab));
	}
	
	
	public void tearDown() throws Exception {
		super.tearDown();
		cv = null;
		poulet = null;
		canard = null;
		paon = null;
	}


}
