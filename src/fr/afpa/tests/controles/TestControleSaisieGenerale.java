package fr.afpa.tests.controles;

import org.junit.jupiter.api.Test;

import fr.afpa.controles.ControleSasieGenerale;
import junit.framework.TestCase;

public class TestControleSaisieGenerale extends TestCase {

	@Test
	public void testControleSaisieEntierValide() {
		String nombre = "23";
		assertTrue("Probleme controleSaisieInt", ControleSasieGenerale.controleSaisieInt(nombre));
	}

	@Test
	public void testControleSaisieEntierNonValide() {
		String nombre = "23.45";
		assertFalse("Probleme controleSaisieInt", ControleSasieGenerale.controleSaisieInt(nombre));
	}
	
	@Test
	public void testControleSaisieDecimaleValide() {
		String nombre = "24.5";
		assertTrue("Probleme controleSaisieFloat", ControleSasieGenerale.controleSaisieFloat(nombre));
	}

	@Test
	public void testControleSaisieDecimaleNonValide() {
		String nombre = "24,5";
		assertFalse("Probleme controleSaisieFloat", ControleSasieGenerale.controleSaisieFloat(nombre));
	}
	
	@Test
	public void testControleSaisieBooleanValideOui() {
		String mot = "oui";
		assertTrue("Probleme controleSaisieBoolean", ControleSasieGenerale.controleSaisieBoolean(mot));
	}
	
	@Test
	public void testControleSaisieBooleanValidNon() {
		String mot = "non";
		assertTrue("Probleme controleSaisieBoolean", ControleSasieGenerale.controleSaisieBoolean(mot));
	}

	@Test
	public void testControleSaisieBooleanNonValide() {
		String mot = "truc";
		assertFalse("Probleme controleSaisieBoolean", ControleSasieGenerale.controleSaisieBoolean(mot));
	}
	
}
