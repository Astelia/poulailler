package fr.afpa.tests.controles;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.controles.ControleVolailleComestible;
import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;
import fr.afpa.entites.VolailleComestible;
import junit.framework.TestCase;

public class TestControleVolailleComestible extends TestCase {

	private VolailleComestible poulet;
	private ControleVolailleComestible cvc;
	
	@BeforeEach
	public void setUp() throws Exception {
		super.setUp();
		Volaille.setCpt(0);
		poulet = new Poulet(2);
		Poulet.setPoidsFinalKilo(10);
		cvc = new ControleVolailleComestible();
	}

	@Test
	public void testPoidsValideOuNon() {
		assertFalse("Probleme controle poids", cvc.poidsPrixValide(-5f));
		assertTrue("Probleme controle poids", cvc.poidsPrixValide(6));
	}
	
	@Test
	public void testVolailleAbattable() {
		assertFalse("Probleme controle abattable", cvc.volailleAbattable(poulet));
		poulet.setAbattable(true);
		assertTrue("Probleme controle abattable", cvc.volailleAbattable(poulet));
	}
	
	@Test
	public void testChangementStatutOuNon() {
		assertFalse("Probleme controle changement statut", cvc.volailleChangeStatutAbattable(poulet));
		poulet.setPoids(11);
		assertTrue("Probleme controle changement statut", cvc.volailleChangeStatutAbattable(poulet));
	}
	
	@Test
	public void testTypeValide() {
		assertTrue("Probleme controle type", cvc.bonTypeVolaille("poulet"));
		assertTrue("Probleme controle type", cvc.bonTypeVolaille("canard"));
	}
	
	@Test
	public void testTypeInValide() {
		assertFalse("Probleme controle type", cvc.bonTypeVolaille("truc"));
	}
	
	@AfterEach
	public void tearDown() throws Exception {
		super.tearDown();
		poulet = null;
		cvc = null;
	}

	

}
