package fr.afpa.tests.controles;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import fr.afpa.controles.ControleElevage;
import fr.afpa.entites.Canard;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;
import junit.framework.TestCase;

public class TestControleElevage extends TestCase {

	private ControleElevage ce;
	private Volaille paon;
	private Volaille poulet;
	private Volaille canard;
	private Map<String, Volaille> listeVolailles;
	
	
	public void setUp() throws Exception {
		super.setUp();
		Volaille.setCpt(0);
		ce = new ControleElevage();
		paon = new Paon();
		poulet = new Poulet();
		canard = new Canard();
		
		listeVolailles = new HashMap<String, Volaille>();
		listeVolailles.put(paon.getNumeroIdentification(), paon);
		listeVolailles.put(poulet.getNumeroIdentification(), poulet);
		listeVolailles.put(canard.getNumeroIdentification(), canard);
	}

	@Test
	public void testVolaillePresenteDanListe() {
		assertTrue("Probleme controle existence volaille dans liste"
					, ce.existenceVolailleParId(paon.getNumeroIdentification(), listeVolailles));
	}
	
	@Test
	public void testVolailleNonPresenteDanListe() {
		assertFalse("Probleme controle existence volaille dans liste", ce.existenceVolailleParId("bidule", listeVolailles));
	}

	
	public void tearDown() throws Exception {
		super.tearDown();
		ce = null;
		paon = null;
		poulet = null;
		canard = null;
		listeVolailles = null;
	}

	
}
