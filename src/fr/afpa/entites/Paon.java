package fr.afpa.entites;

public final class Paon extends Volaille {

	public static final int MAX_PAR_LIVRAISON = 3;
	
	public static final String ORIGINE = "parc naturel";
	public static final String GARDE_J_USQU_A = "a tout moment";
	
	
	public Paon() {
		super();
	}

	
	@Override
	public final int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numeroIdentification == null) ? 0 : numeroIdentification.hashCode());
		return result;
	}

	@Override
	public final boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Paon other = (Paon) obj;
		if (numeroIdentification == null) {
			if (other.numeroIdentification != null)
				return false;
		} else if (!numeroIdentification.equals(other.numeroIdentification))
			return false;
		return true;
	}
	@Override
	public final String toString() {
		return "Paon [numeroIdentification=" + numeroIdentification + "]";
	}
	
}
