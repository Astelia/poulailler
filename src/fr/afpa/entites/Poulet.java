package fr.afpa.entites;

import fr.afpa.services.VolailleComestibleServices;

public final class Poulet extends VolailleComestible {

	public static final int MAX_PAR_LIVRAISON = 5;
	
	private static float prix;
	private static float poidsFinalKilo;
	
	
	public Poulet() {
		super();
	}

	public Poulet(float poids) {
		super(poids);
		VolailleComestibleServices vcs = new VolailleComestibleServices();
		vcs.modificationStatutAbattable(this);
	}
	

	public static float getPrix() {
		return prix;
	}

	public static void setPrix(float prix) {
		Poulet.prix = prix;
	}

	public static float getPoidsFinalKilo() {
		return poidsFinalKilo;
	}

	public static void setPoidsFinalKilo(float poidsFinalKilo) {
		Poulet.poidsFinalKilo = poidsFinalKilo;
	}

	
	@Override
	public final int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numeroIdentification == null) ? 0 : numeroIdentification.hashCode());
		result = prime * result + Float.floatToIntBits(poids);
		result = prime * result + (abattable ? 1231 : 1237);
		return result;
	}

	@Override
	public final boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Poulet other = (Poulet) obj;
		if (numeroIdentification == null) {
			if (other.numeroIdentification != null)
				return false;
		}
		else if (!numeroIdentification.equals(other.numeroIdentification))
			return false;
		if (Float.floatToIntBits(poids) != Float.floatToIntBits(other.poids))
			return false;
		if (abattable != other.abattable)
			return false;
		return true;
	}
	
	@Override
	public final String toString() {
		return "Poulet [numeroIdentification=" + numeroIdentification + ", poids=" + poids + ", abattable=" + abattable 
				+ "]";
	}
	
	
}
