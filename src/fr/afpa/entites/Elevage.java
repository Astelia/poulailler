package fr.afpa.entites;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Elevage {

	private String nom;
	private Eleveur eleveur;
	private Map<String, Volaille> listeVolailles;
	private float chiffreDAffaire;
	private Scanner scanner;

	
	public Elevage() {
		super();
		this.nom = "Le Gaulois";
		this.eleveur = new Eleveur();
		this.listeVolailles = new HashMap<String, Volaille>();
	}

	public Elevage(String nom, Scanner scanner) {
		super();
		this.nom = nom;
		this.scanner = scanner;
		this.eleveur = new Eleveur();
		this.listeVolailles = new HashMap<String, Volaille>();
	}

	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Eleveur getEleveur() {
		return eleveur;
	}

	public void setEleveur(Eleveur eleveur) {
		this.eleveur = eleveur;
	}

	public Map<String, Volaille> getListeVolailles() {
		return listeVolailles;
	}

	public void setListeVolailles(Map<String, Volaille> listeVolailles) {
		this.listeVolailles = listeVolailles;
	}

	public float getChiffreDAffaire() {
		return chiffreDAffaire;
	}

	public void setChiffreDAffaire(float chiffreDAffaire) {
		this.chiffreDAffaire = chiffreDAffaire;
	}

	public Scanner getScanner() {
		return scanner;
	}

	public void setScanner(Scanner scanner) {
		this.scanner = scanner;
	}

	
	@Override
	public String toString() {
		return "Elevage [nom=" + nom + ", eleveur=" + eleveur + ", listeVolailles=" + listeVolailles
				+ ", chiffreDAffaire=" + chiffreDAffaire + "]";
	}
	
}
