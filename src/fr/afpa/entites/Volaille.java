package fr.afpa.entites;

public abstract class Volaille implements Comparable {

	public static final int NB_MAX_PAR_LIVRAISON = 7;
	public static final int AGE_D_ARRIVEE_EN_SEMAINE = 3;
	
	private static int cpt = 0;
	
	protected final String numeroIdentification;


	public Volaille() {
		super();
		this.numeroIdentification = String.format("%07d", ++cpt);
	}

	
	public static int getCpt() {
		return cpt;
	}

	public static void setCpt(int cpt) {
		Volaille.cpt = cpt;
	}

	public String getNumeroIdentification() {
		return numeroIdentification;
	}

	
	/**
	 * On compare par rapport aux identifiants
	 */
	@Override
	public int compareTo(Object o) {
		Volaille volaille = (Volaille) o;
		return this.numeroIdentification.compareTo(volaille.numeroIdentification);
	}

	@Override
	public abstract int hashCode();

	@Override
	public abstract boolean equals(Object obj);

	@Override
	public abstract String toString();
	
	
	
}
