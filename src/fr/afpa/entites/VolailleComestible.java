package fr.afpa.entites;

public abstract class VolailleComestible extends Volaille {

	public static final String ORIGINE = "fournisseur";
	public static final String GARDE_J_USQU_A = "taille necessaire a la commercialisation";
	
	
	protected float poids;
	protected boolean abattable;
	
	
	public VolailleComestible() {
		super();
	}

	public VolailleComestible(float poids) {
		super();
		this.poids = poids;
	}
	

	public float getPoids() {
		return poids;
	}

	public void setPoids(float poids) {
		this.poids = poids;
	}

	public boolean isAbattable() {
		return abattable;
	}

	public void setAbattable(boolean abattable) {
		this.abattable = abattable;
	}

	
	/**
	 * On compare par rapport au poids actuel de la volaille comestible
	 */
	@Override
	public int compareTo(Object o) {
		if (this.poids < ((VolailleComestible) o).poids) {
			return -1;
		}
		else if (this.poids > ((VolailleComestible) o).poids) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public abstract int hashCode();

	@Override
	public abstract boolean equals(Object obj);

	@Override
	public abstract String toString();
	
	
	
}
