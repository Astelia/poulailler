package fr.afpa.controles;

public class ControleSasieGenerale {

	/**
	 * Permet de verifier si une chaine de caractere represente un float
	 * @param nombre : String dont on verifie la conversion en float
	 * @return true si convertible en float et false sinon
	 */
	public static boolean controleSaisieFloat(String nombre) {
		try {
			Float.parseFloat(nombre);
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * Permet de verifier si une chaine de caractere represente un float
	 * @param nombre : String dont on verifie la conversion en float
	 * @return true si convertible en entier et false sinon
	 */
	public static boolean controleSaisieInt(String nombre) {
		try {
			Integer.parseInt(nombre);
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * Permet de verifier si une chaine de caractere est egale a "oui" ou "non"
	 * @param bool : String dont que l'on compare a "oui" et/ou "non"
	 * @return true si le string est egal a "oui" ou "non" et false sinon
	 */
	public static boolean controleSaisieBoolean(String bool) {
		return "oui".equalsIgnoreCase(bool) || "non".equalsIgnoreCase(bool);
	}
	
}
