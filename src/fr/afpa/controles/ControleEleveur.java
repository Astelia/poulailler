package fr.afpa.controles;

import fr.afpa.entites.Eleveur;

public class ControleEleveur {

	/**
	 * Permet de verifier si le login et le mot de passe passes en parametre correspondent
	 * au login et au mot de passe de l'eleveur passe en parametre
	 * @param eleveur : eleveur avec le on compare le login et le mot de passe
	 * @param login : String a compare
	 * @param mdp : String a compare
	 * @return true si le login et le mot de passe sont corrects et false sinon
	 */
	public boolean verificationLoginMdp(Eleveur eleveur, String login, String mdp) {
		return eleveur.getLogin().equals(login) && eleveur.getMotDePasse().equals(mdp);
	}
	
}
