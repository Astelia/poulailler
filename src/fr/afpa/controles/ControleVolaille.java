package fr.afpa.controles;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;
import fr.afpa.entites.VolailleComestible;

public class ControleVolaille {

	/**
	 * Permet de verifier si l'identifiant est valide
	 * @param id : String a verifier
	 * @return true si id est une chaine de caracteres de 8 chiffres et false sinon
	 */
	public boolean identifiantValide(String id) {
		return id.matches("[\\d]{7}");
	}
	
	/**
	 * Permet de verifier si une volaille est comestible ou non
	 * @param volaille : la volaille comestible ou non
	 * @return true si la volaille est comestible et false sinon
	 */
	public boolean volailleEstComestible(Volaille volaille) {
		return volaille instanceof VolailleComestible;
		//return volaille instanceof Poulet || volaille instanceof Canard;
	}
	
	/**
	 * Permet de verifier si chaque nombre ne depasse pas le nombre max par livraison de chaque type de volaille
	 * @param tabNombre : le tableau des nombres de volailles de chaque type livrees
	 * @return true si les nombres ne depasse pas les nombres max et false sinon
	 */
	public boolean nbMaxParLivraisonTypeVolaillePasDepasse(int[] tabNombre) {
		if (tabNombre.length == 3) {
			int nbVolailles = 0;
			for (int i = 0; i < tabNombre.length; i++) {
				nbVolailles += tabNombre[i];
			}
			return nbVolailles<=Volaille.NB_MAX_PAR_LIVRAISON 
					&& tabNombre[0]<=Poulet.MAX_PAR_LIVRAISON
					&& tabNombre[1]<= Canard.MAX_PAR_LIVRAISON
					&& tabNombre[2]<= Paon.MAX_PAR_LIVRAISON;
		}
		else {
			return false;
		}
	}
	
}
