package fr.afpa.controles;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Poulet;
import fr.afpa.entites.VolailleComestible;

public class ControleVolailleComestible {

	/**
	 * Permet de verifier si la volaille comestible passe en parametre est abattable ou non
	 * @param volailleC : la volaille comestible
	 * @return : true si la volaille est abattable et false sinon
	 */
	public boolean volailleAbattable(VolailleComestible volailleC) {
		return volailleC.isAbattable();
	}
	
	/**
	 * Permet de verifier si le poids ou le prix passe en parametre est strictement positif ou non
	 * @param poidsPrix : poids ou prix en float
	 * @return true si le poids ou le prix est strictement positif et false sinon
	 */
	public boolean poidsPrixValide(float poidsPrix) {
		return 0 < poidsPrix; 
	}
	
	/**
	 * Permet de verifier si une volaille comestible doit changer son statut d'abattable ou non
	 * @param volailleC : la volaille a verifier
	 * @return true si elle doit changer de statut et false sinon
	 */
	public boolean volailleChangeStatutAbattable(VolailleComestible volailleC) {
		if ((volailleC instanceof Poulet && volailleC.getPoids() >= Poulet.getPoidsFinalKilo()) 
			|| (volailleC instanceof Canard && volailleC.getPoids() >= Canard.getPoidsFinalKilo())) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Permet de verifier si le type passe en parametre est "poulet" ou "canard"
	 * @param type : le String a comparer
	 * @return true si type est égal a "poulet" ou "canard"
	 */
	public boolean bonTypeVolaille(String type) {
		return "poulet".equals(type) || "canard".equals(type);
	}
	
}
