package fr.afpa.controles;

import java.util.Map;

import fr.afpa.entites.Volaille;

public class ControleElevage {

	/**
	 * Permet de verifier si une volaille est presente dans une liste de volailles
	 * grace a son identifiant
	 * @param listeVolailles : liste dans la quelle on cherche la volaille
	 * @param idVolaille : identifiant de la volaille a trouve
	 * @return true si la volaille est presente dans laiste et false sinon
	 */
	public boolean existenceVolailleParId( String idVolaille, Map<String, Volaille> listeVolailles) {
		return listeVolailles.containsKey(idVolaille);
	}
	
}
