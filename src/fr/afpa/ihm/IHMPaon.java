package fr.afpa.ihm;

public class IHMPaon {

	/**
	 * Permet d'afficher que le paon a ete rendu a un parc.
	 * @param numId   : identifiant du paon a rendre
	 * @param nomParc : nom du parc auquel on rend le paon
	 */
	public void paonRendu(String numId, String nomParc) {
		System.out.println("Le paon dont l'identifiant est "+numId+" a ete rendu au parc "+nomParc+".");
	}
	
}
