package fr.afpa.ihm;

import java.util.Scanner;

public class IHMVolaille {
	
	/**
	 * Permet de saisir l'identifiant d'une volaille
	 * @param scanner : source de la saisie
	 * @return l'identifiant de la volaille recherchee
	 */
	public String demanderIdentifiantVolaille(Scanner scanner) {
		System.out.print("Veuillez entrer l'identifiant de la volaille recherchee : ");
		int id = SaisieGenerale.saisieInt(scanner);
		return String.format("%07d", id);
	}
	
}
