package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.controles.ControleVolailleComestible;

public class IHMVolailleComestible {

	/**
	 * Permet d'afficher la confirmation que la volaille comestible a bien ete vendue
	 * @param idVolailleC : identifiant volaille comestible
	 */
	public void affichageConfirmationVolailleComestibleVendue(String idVolailleC) {
		System.out.println("La volaille comestible d'identifiant "+idVolailleC+" a bien ete vendue.");
	}
	
	/**
	 * Permet de confirmer la modification du poids actuel ou du poids d'abattage
	 * ainsi que la nouvelle valeur
	 * @param poids : le poids modifie
	 * @param typePoids : "actuel" ou "d'abattage"
	 */
	public void affichageConfirmationModificationPoids(float poids, String typePoids) {
		System.out.println("Le poids "+typePoids+" a bien ete modifie.");
		System.out.println("Il est maintenant de "+poids+" kilo(s).");
	}
	
	/**
	 * Permet de confirmer la modification du prix d'une volaille ainsi que sa nouvelle valeur
	 * @param prix : le prix modifie
	 * @param type : "poulet" ou "canard"
	 */
	public void affichageConfirmationModificationDuPrix(float prix, String type) {
		System.out.println("Le prix du "+type+" a bien ete modifie.");
		System.out.println("Il est maintenant de "+prix+" euro(s).");
	}
	
	/**
	 * Permet de saisir le type de volaille comestible on souhaite choisir
	 * @param scanner : source de la saisie
	 * @return <<poulet>> ou <<canard>>
	 */
	public String demndeSaisieChoix(Scanner scanner) {
		ControleVolailleComestible cvc = new ControleVolailleComestible();
		System.out.println("Pour quel type de volaille (poulet/canard) : ");
		String choix = SaisieGenerale.saisieMot(scanner);
		while (!cvc.bonTypeVolaille(choix)) {
			System.out.println("Vous devez entrer <<poulet>> ou <<canard>> !");
			return demndeSaisieChoix(scanner);
		}
		return choix;
	}
	
	/**
	 * Permet de saisir le nouveau poids actuel ou d'abattage
	 * @param scanner : source de la saisie
	 * @return le nouveau poids actuel ou d'abattage
	 */
	public float demandeSaisieNouveauPoids(Scanner scanner) {
		ControleVolailleComestible cvc = new ControleVolailleComestible();
		System.out.print("Veuillez entrer le nouveau poids : ");
		float poids = SaisieGenerale.saisieFloat(scanner);
		while (!cvc.poidsPrixValide(poids)) {
			System.out.println("Le poids est negatif !");
			return demandeSaisieNouveauPoids(scanner);
		}
		return poids;
	}
	
	/**
	 * Permet de saisir le nouveau prix de la volaille
	 * @param scanner : source de la saisie
	 * @return le nouveau prix de la volaille
	 */
	public float demandeSaisieNouveauPrix(Scanner scanner) {
		ControleVolailleComestible cvc = new ControleVolailleComestible();
		System.out.print("Veuillez entrer le nouveau prix : ");
		float prix = SaisieGenerale.saisieFloat(scanner);
		while (!cvc.poidsPrixValide(prix)) {
			System.out.println("Le prix est negatif !");
			return demandeSaisieNouveauPrix(scanner);
		}
		return prix;
	}
	
	/**
	 * Permet d'afficher si une volaille est abattable ou non
	 * @param id : identifiant de la volaille
	 * @param bool : true si abattable et false sinon
	 */
	public void affichageChangementDeStatutAbattable(String id, boolean bool) {
		if (bool) {
			System.out.println("La volaille d'identifiant "+id+" est maintenant abattable.");
		}
		else {
			System.out.println("La volaille d'identifiant "+id+" n'est pas encore abattable.");
		}
		
	}
	
}
