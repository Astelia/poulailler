package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.entites.Eleveur;

public class IHMEleveur {

	/**
	 * Permet d'afficher la confirmation des modifications des informations de l'eleveur
	 * ainsi que ses nouvelle informations.
	 * @param eleveur
	 */
	public void affichageConfirmationModificationEleveur(Eleveur eleveur) {
		System.out.println("Vos informations ont bien ete modifiees.");
		System.out.println("Voici vos nouvelles informations : "+eleveur);
	}
	
	/**
	 * Permet de saisir le nouveau nom de l'eleveur
	 * @param scanner : source de la saisie
	 * @return le nouveau nom
	 */
	public String demanderSaisieNom(Scanner scanner) {
		System.out.print("Veuillez entrer le nouveau nom : ");
		return SaisieGenerale.saisiePhrase(scanner);
	}
	
	/**
	 * Permet de saisir le nouveau prenom de l'eleveur
	 * @param scanner : source de la saisie
	 * @return le nouveau prenom
	 */
	public String demanderSaisiePrenom(Scanner scanner) {
		System.out.print("Veuillez entrer le nouveau prenom : ");
		return SaisieGenerale.saisiePhrase(scanner);
	}
	
	/**
	 * Permet de saisir le nouveau login de l'eleveur
	 * @param scanner : source de la saisie
	 * @return le nouveau login
	 */
	public String demanderSaisieLogin(Scanner scanner) {
		System.out.print("Veuillez entrer le login : ");
		return SaisieGenerale.saisieMot(scanner);
	}
	
	/**
	 * Permet de saisir le nouveau mot de passe de l'eleveur
	 * @param scanner : source de la saisie
	 * @return le nouveau mot de passe
	 */
	public String demanderSaisieMDP(Scanner scanner) {
		System.out.print("Veuillez entrer le mot de passe : ");
		return SaisieGenerale.saisieMot(scanner);
	}
	
}
