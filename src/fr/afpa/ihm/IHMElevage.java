package fr.afpa.ihm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import fr.afpa.controles.ControleElevage;
import fr.afpa.controles.ControleVolaille;
import fr.afpa.entites.Canard;
import fr.afpa.entites.Elevage;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;
import fr.afpa.services.ElevageServices;

public class IHMElevage {

	private static final String MONNAIE = " euro(s).";
	private static final String LIGNE = " ------------------------------------------------------------- ";
	
	/**
	 * Permet d'afficher le nombre de poulets, le nombre de canards et le nombre de paons d'un elevage
	 * @param nom : nom de l'elevage
	 * @param nbsVolaillesParType tableau de nombre de chaque type de volailles
	 */
	public void affichageNombreVolaillesParType(String nom, int[] nbsVolaillesParType) {
		System.out.println("Etat de l'elevage "+nom);
		System.out.println("Nombre de Poulets : "+nbsVolaillesParType[0]);
		System.out.println("Nombre de Canards : "+nbsVolaillesParType[1]);
		System.out.println("Nombre de Paons   : "+nbsVolaillesParType[2]);
	}
	
	/**
	 * Permet d'afficher graphiquement toutes les volailles de la liste
	 * @param listeVolailles : liste de volailles geree
	 */
	public void afficherRepresentationGraphiqueVolaillesParType(Map<String, Volaille> listeVolailles) {
		affichagePouletsCanards(listeVolailles);
		affichagePaons(listeVolailles);
	}
	
	/**
	 * Permet d'afficher graphiquement tous les poulets et les canards de la liste
	 * @param listeVolailles : liste de volailles geree
	 */
	private void affichagePouletsCanards(Map<String, Volaille> listeVolailles) {
		ElevageServices es = new ElevageServices();
		ArrayList<ArrayList<String>> dessin = es.representationGraphiqueElevage(listeVolailles);
		System.out.println(LIGNE);
		
		while (dessin.get(0).size()>=10 && dessin.get(1).size()>=10) {
			System.out.print("| ");
			for (int i=1; i<=10; i++) {
				System.out.print(dessin.get(0).remove(0));
			}
			System.out.print(" | ");
			for (int i=1; i<=10; i++) {
				System.out.print(dessin.get(1).remove(0));
			}
			System.out.println("|");
		}
		
		if (dessin.get(0).size() <= dessin.get(1).size()) {
			System.out.print("| ");
			for (String string : dessin.get(0)) {
				System.out.print(string);
			}
			for (int i=1; i<=10-dessin.get(0).size(); i++) {
				System.out.print("   ");
			}
			
			while (dessin.get(1).size()>=10) {
				System.out.print("| ");
				for (int i=1; i<=10; i++) {
					System.out.print(dessin.get(1).remove(0));
				}
				System.out.println("|");
			}
			System.out.print("| ");
			for (String string : dessin.get(1)) {
				System.out.print(string);
			}
			for (int i=1; i<=10-dessin.get(1).size(); i++) {
				System.out.print("   ");
			}
			System.out.println("|");
		}
		else {
			System.out.print("| ");
			for (String string : dessin.get(1)) {
				System.out.print(string);
			}
			for (int i=1; i<=10-dessin.get(1).size(); i++) {
				System.out.print("   ");
			}
			
			while (dessin.get(0).size()>=10) {
				System.out.print("| ");
				for (int i=1; i<=10; i++) {
					System.out.print(dessin.get(0).remove(0));
				}
				System.out.println("|");
			}
			System.out.print("| ");
			for (String string : dessin.get(0)) {
				System.out.print(string);
			}
			for (int i=1; i<=10-dessin.get(0).size(); i++) {
				System.out.print("   ");
			}
			System.out.println("|");
		}
	}
	
	/**
	 * Permet d'afficher graphiquement tous les paons de la liste
	 * @param listeVolailles : liste de volailles geree
	 */
	private void affichagePaons(Map<String, Volaille> listeVolailles) {
		ElevageServices es = new ElevageServices();
		ArrayList<ArrayList<String>> dessin = es.representationGraphiqueElevage(listeVolailles);
		
		System.out.println(LIGNE);
		while (dessin.get(2).size()>=20) {
			System.out.print("| ");
			for (int i=1; i<=20; i++) {
				System.out.print(dessin.get(2).remove(0));
			}
			System.out.println("|");
		}
		System.out.print("| ");
		for (String string : dessin.get(2)) {
			System.out.print(string);
		}
		for (int i=1; i<=20-dessin.get(2).size(); i++) {
			System.out.print("   ");
		}
		System.out.println("|");
		System.out.println(LIGNE);
	}
	
	/**
	 * Permet d'afficher le prix total des volailles abattables d'un elevage
	 * @param prixTotal : en euros
	 */
	public void affichageTotalPrixVolaillesAbattables(float prixTotal) {
		System.out.println("Le total des prix des volaille abattables est de "+prixTotal+MONNAIE);
	}
	
	/**
	 * Permet d'afficher une livraison ainsi que son deroulement
	 * @param nombresVolaillesParType : nombres de volailles par type
	 */
	public void confirmationLivraisonEffectuee(int[] nombresVolaillesParType) {
		ControleVolaille cv = new ControleVolaille();
		if (cv.nbMaxParLivraisonTypeVolaillePasDepasse(nombresVolaillesParType)) {
			System.out.println("La livraison a bien ete effectuee.");
			System.out.println(nombresVolaillesParType[0]+" poulets, "
							  +nombresVolaillesParType[1]+" canards et "
							  +nombresVolaillesParType[2]+ " paons ont etes livres.");
		}
		else {
			System.out.println("La livraison n'est pas valide.");
		}
	}
	
	/**
	 * Permet de saisir le nouveau nom d'un elevage
	 * @param scanner : source de la saisie
	 * @return le nouveau nom
	 */
	public String demanderSaisieNouveauNom(Scanner scanner) {
		System.out.print("Quel est le nouveau nom de l'elevage ? : ");
		return SaisieGenerale.saisiePhrase(scanner);
	}
	
	/**
	 * Permet de saisir le nombre de volailles que l'on souhaite ajouter pour un type
	 * @param type : le type de la volaille
	 * @param scanner : source de la saisie
	 * @return le nombre de volailles a ajouter
	 */
	public int demanderNombreDeVolailles(String type, Scanner scanner) {
		System.out.print("Combien de "+type+" voulez-vous ajouter ? : ");
		return SaisieGenerale.saisieInt(scanner);
	}
	
	/**
	 * Permet d'afficher l'argent gagne et le chiffre d'affaire qui en decoule
	 * @param montantAjoute : argent gagne
	 * @param chiffreDAf : le nouveau chiffre d'affaire
	 */
	public void afficheageArgentAjouteEtChiffreDAffaire(float montantAjoute, float chiffreDAf) {
		System.out.println("Cette vente vous rapporte "+montantAjoute+MONNAIE);
		System.out.println("Le nouveau chiffre d'affaire est de "+chiffreDAf+MONNAIE);
	}
	
	/**
	 * Permet d'afficher qu'une volaille existe ou pas
	 * @param numId      : identifiant de la volaille
	 * @param elevage : l'elevage dans lequel on recherche la volaille
	 */
	public void volailleExisteOuPas(String numId, Elevage elevage) {
		ControleElevage ce = new ControleElevage();
		if (ce.existenceVolailleParId(numId, elevage.getListeVolailles())) {
			System.out.println("La volaille "+numId+" est bien dans le poulailler "+elevage.getNom()+".");
		}
		else {
			System.out.println("La volaille "+numId+" n'est pas dans le poulailler "+elevage.getNom()+".");
		}
	}
	
	/**
	 * Permet de creer une livraison a partir d'une saisie
	 * @param nbsParType : tableau nombres de volailles par type
	 * @param scanner : source de la saisie
	 * @return la liste de volailles cree
	 */
	public Map<String, Volaille> creationLivraison(int[] nbsParType, Scanner scanner) {
		Map<String, Volaille> listeVolailles = new HashMap<>();
		IHMVolailleComestible ihmVC = new IHMVolailleComestible();
		for (int i=1; i<=nbsParType[0]; i++) {
			Volaille volaille = new Poulet(ihmVC.demandeSaisieNouveauPoids(scanner));
			listeVolailles.put(volaille.getNumeroIdentification(), volaille);
		}
		for (int i=1; i<=nbsParType[1]; i++) {
			Volaille volaille = new Canard(ihmVC.demandeSaisieNouveauPoids(scanner));
			listeVolailles.put(volaille.getNumeroIdentification(), volaille);
		}
		for (int i=1; i<=nbsParType[2]; i++) {
			Volaille volaille = new Paon();
			listeVolailles.put(volaille.getNumeroIdentification(), volaille);
		}
		return listeVolailles;
	}
	
}
