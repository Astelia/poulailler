package fr.afpa.ihm;

import java.util.Scanner;

public class IHMMenu {

	/**
	 * Affichage du menu
	 */
	public static void affichageMenu() {
		System.out.println("______________________________________________________________");
		System.out.println();
		System.out.println("A - Modifier le nom de l'exploitation");
		System.out.println("B - Modifier le profil de l'eleveur");
		System.out.println("C - Afficher le nombre de volailles par type avec dessin");
		System.out.println("D - Afficher le total des prix des volailles abattables");
		System.out.println("E - Enregister de nouvelles volailles");
		System.out.println("F - Modifier le poids d'abattage d'une volaille comestible");
		System.out.println("G - Modifier le prix du jour d'une volaille abattable");
		System.out.println("H - Modifier le poids d'une volaille comestible");
		System.out.println("I - Vendre une volaille comestible");
		System.out.println("J - Rendre une volaille non comestible a un parc");
		System.out.println("Q - Quitter");
		System.out.println("______________________________________________________________");
	}
	
	/**
	 * Affichage des entetes
	 * @param entete : titre de l'entete
	 */
	public static void entetesMenu(String entete) {
		System.out.println();
		System.out.println();
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		System.out.println(entete);
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	
	public static String demanderSaisirChoix(Scanner scanner) {
		System.out.print("Veuillez saisir votre choix : ");
		return SaisieGenerale.saisieMot(scanner);
	}
	
}
