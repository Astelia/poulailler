package fr.afpa.ihm;

import java.util.Scanner;

public class IHMGeneral {

	/**
	 * Permet de verifier si l'utilisateur veut continuer son operation
	 * @param scanner : le flux de la reponse
	 * @return true s'il veut poursuivre et false sinon
	 */
	public static boolean demanderSaisieConfirmationPoursuite(Scanner scanner) {
		System.out.print("Souhaitez-vous poursuivre l'operation (oui non) : ");
		return SaisieGenerale.saisieBoolean(scanner);
	}
	
}
