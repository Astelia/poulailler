package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.controles.ControleSasieGenerale;

public class SaisieGenerale {

	/**
	 * Permet de recuperer une chaine caracteres saisie sans espace 
	 * @param scanner : la source du String
	 * @return la chaine de caractere saisie sans espace
	 */
	public static String saisieMot(Scanner scanner) {
		String mot = scanner.next();
		scanner.nextLine();
		return mot;
	}
	
	/**
	 * Permet de recuperer une chaine de caracteres saisie avec des espaces
	 * @param scanner : la source du String
	 * @return la chaine de caractere saisie avec espace(s)
	 */
	public static String saisiePhrase(Scanner scanner) {
		return scanner.nextLine();
	}
	
	/**
	 * Permet de recuperer un float saisi en String
	 * @param scanner : la source du String
	 * @return la conversion en float du String saisi
	 */
	public static float saisieFloat(Scanner scanner) {
		String nombre = scanner.next();
		scanner.nextLine();
		while (!ControleSasieGenerale.controleSaisieFloat(nombre)) {
			System.out.println("Veuillez entrer un nombre decimal valide !");
			return saisieFloat(scanner);
		}
		return Float.parseFloat(nombre);
	}
	
	/**
	 * Permet de recuperer un entier saisi en String
	 * @param scanner : la source du String
	 * @return la conversion en entier du String saisi
	 */
	public static int saisieInt(Scanner scanner) {
		String nombre = scanner.next();
		scanner.nextLine();
		while (!ControleSasieGenerale.controleSaisieInt(nombre)) {
			System.out.println("Veuillez entrer un nombre entier valide !");
			return saisieInt(scanner);
		}
		return Integer.parseInt(nombre);
	}
	
	/**
	 * Permet de recuperer un booleen saisi en String
	 * @param scanner : la source du String
	 * @return la conversion en boolean du String saisi
	 */
	public static boolean saisieBoolean(Scanner scanner) {
		String bool = scanner.next();
		scanner.nextLine();
		while (!ControleSasieGenerale.controleSaisieBoolean(bool)) {
			System.out.println("Veuillez choisir entre <<oui>> et <<non>> !");
			return saisieBoolean(scanner);
		}
		if ("oui".equals(bool)) {
			return true;
		}
		else {
			return false;
		}
	}
	
}
